FROM openjdk:8-jre-alpine


ADD *.jar /app/app.jar

CMD java -jar /app/app.jar

EXPOSE 8080
